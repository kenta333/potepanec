module ApplicationHelper
  # ページごとの完全なタイトルを返します。
  MAIN_TITLE = "BIGBAG Store".freeze
  def full_title(page_title = '')
    if page_title.blank?
      MAIN_TITLE
    else
      "#{page_title} - #{MAIN_TITLE}"
    end
  end
end
