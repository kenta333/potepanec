require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:other_product) { create(:product) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "HTTP接続が成功する" do
      expect(response).to have_http_status(:success)
    end

    it "@taxonomies、@taxon、@productsに正しい値が入る" do
      expect(assigns(:taxonomies).first).to eq taxonomy
      expect(assigns(:taxon)).to eq taxon
      expect(assigns(:products)).to eq taxon.all_products
    end

    it "@taxonに紐づかない商品は、@productsに入らない" do
      expect(assigns(:products)).not_to include other_product
    end
  end
end
