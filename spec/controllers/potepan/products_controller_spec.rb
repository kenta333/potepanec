require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let!(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "HTTP接続が成功する" do
      expect(response).to have_http_status(:success)
    end

    it "@productに正しい値が入る" do
      expect(assigns(:product)).to eq product
    end

    it "正しいテンプレートが呼び出される" do
      expect(response).to render_template :show
    end
  end
end
