require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_titleメソッド" do
    subject { helper.full_title(title) }

    context "引数に値がある場合" do
      let(:title) { "Example" }

      it { is_expected.to eq "#{title} - BIGBAG Store" }
    end

    context "引数がblankの場合" do
      let(:title) { "" }

      it { is_expected.to eq "BIGBAG Store" }
    end

    context "引数がnilの場合" do
      let(:title) { nil }

      it { is_expected.to eq "BIGBAG Store" }
    end
  end
end
