require 'rails_helper'

RSpec.describe "categoriesのsystem specテスト", type: :system do
  describe "リンクのテスト" do
    let!(:taxon) { create(:taxon, name: "taxon_example_1") }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:other_taxon) { create(:taxon, name: "taxon_example_2") }
    let!(:other_product) { create(:product, taxons: [other_taxon]) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "lightsectionのHomeボタンをクリックしたら、indexページに移動する" do
      find("#light_section_home_btn").click
      expect(current_path).to eq potepan_path
    end

    it "headerのHomeボタンをクリックしたら、indexページに移動する" do
      find("#header_home_btn").click
      expect(current_path).to eq potepan_path
    end

    it "ページ中央にカテゴリに紐づいた商品を表示する" do
      within("#rspec_each_product_info") do
        expect(page).to have_content product.name
      end
    end

    it "ページのカテゴリに紐づかない商品名を表示しない" do
      expect(page).not_to have_content other_product.name
    end

    it "ページ左ににカテゴリ一覧を表示する" do
      within("#rspec_category_selecter") do
        expect(page).to have_content(taxon.name, other_taxon.name)
      end
    end

    it "別のカテゴリー選択時、対象のカテゴリーページに移動する" do
      click_link other_taxon.name
      expect(current_path).to eq potepan_category_path(other_taxon.id)
    end
  end
end
