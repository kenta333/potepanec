require 'rails_helper'

RSpec.describe "productsのsystem specテスト", type: :system do
  describe "リンクのテスト" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it "lightsectionのHomeボタンをクリックしたら、indexページに移動する" do
      find("#light_section_home_btn").click
      expect(current_path).to eq potepan_path
    end

    it "headerのHomeボタンをクリックしたら、indexページに移動する" do
      find("#header_home_btn").click
      expect(current_path).to eq potepan_path
    end

    it "「一覧ページへ戻る」ボタンをクリックしたら、対象商品の属するカテゴリーページに移動する" do
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end
end
